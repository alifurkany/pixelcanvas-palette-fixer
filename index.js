const Jimp = require("jimp");

const palette = [
	0xffffff, 0xe4e4e4, 0x888888, 0x222222, 0xffa7d1, 0xe50000, 0xe59500,
	0xa06a42, 0xe5d900, 0x94e044, 0x02be01, 0x00d3dd, 0x0083c7, 0x0000ea,
	0xcf6ee4, 0x820080,
].map((c) => c * 256 + 255);

// 0xrrggbbaa
const red = (x) => (x & 0xff000000) >>> 24;
const green = (x) => (x & 0x00ff0000) >>> 16;
const blue = (x) => (x & 0x0000ff00) >>> 8;
const alpha = (x) => x & 0x000000ff;
const rgba = (x) => [red(x), green(x), blue(x), alpha(x)];

const distance = (c1, c2) =>
	Math.sqrt(
		Math.pow(c1[0] - c2[0], 2) +
			Math.pow(c1[1] - c2[1], 2) +
			Math.pow(c1[2] - c2[2], 2)
	);

/**
 * @param {Jimp} canvas
 * @returns {Jimp}
 */
function fixPalette(canvas) {
	// for every pixel
	for (let px = 0; px < canvas.bitmap.width; px++) {
		for (let py = 0; py < canvas.bitmap.height; py++) {
			let col = canvas.getPixelColor(px, py);
			if (col !== 0x00000000) {
				// replace with closest color in the palette
				canvas.setPixelColor(
					palette.sort(
						(a, b) =>
							distance(rgba(col), rgba(a)) -
							distance(rgba(col), rgba(b))
					)[0],
					px,
					py
				);
			}
		}
	}
	return canvas;
}

if (process.argv.length !== 3) {
	console.error(`Usage: node . <FILE>`);
	process.exit(1);
} else {
	Jimp.read(process.argv[2])
		.then(fixPalette)
		.then((canvas) => canvas.write(process.argv[2]));
}
