# pixelcanvas-palette-fixer

converts an image to the pixelcanvas palette

## Installation

```bash
git clone https://gitlab.com/alifurkany/pixelcanvas-palette-fixer
cd pixelcanvas-palette-fixer
npm install
```

## Usage

```bash
node . <FILE>
```
